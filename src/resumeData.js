export const resumeData = {
  main: {
    name: "Patrick Figueroa",
    occupation: "FullStack Developer con 2 años de experiencia",
    description: "React | Vue Js | Express | Firebase | MongoDB | MySQL | AWS",
    image: "patrick2.jpg",
    bio:
      "Mi nombre es Patrick Figueroa. Soy FullStack Developer. Actualmente trabajo en Indra como Analista Técnico | Frontend Developer en el día y como freelancer en la noche en Onfire.co como FullStack Developer",
    contactmessage:
      "Si tiene alguna duda o consulta, no dude en contactarse conmigo. Saludos 🚀",
    email: "patrickcsk1@gmail.com",
    phone: "+51 959 026 115",
    address: {
      street: "Jr. Miguel de Unamuno 225",
      city: "Lima, Perú",
      state: "PE",
      zip: "15086",
    },
    website: "http://www.timbakerdev.com",
    resumedownload:
      "https://patrick-figueroa.s3.amazonaws.com/CV+Patrick+Figueroa.pdf",
    social: [
      {
        name: "facebook",
        url: "https://www.facebook.com/patrick.anderson01",
        className: "fa fa-facebook",
      },
      {
        name: "linkedin",
        url: "https://www.linkedin.com/in/patrick-anderson-figueroa-lopez/",
        className: "fa fa-linkedin",
      },
      {
        name: "instagram",
        url: "https://www.instagram.com/patrick.a.fl/",
        className: "fa fa-instagram",
      },
      {
        name: "github",
        url: "https://github.com/patrickcsk1",
        className: "fa fa-github",
      },
    ],
  },
  resume: {
    skillmessage:
      "Con experiencia obtenida de empresas dedicadas a la parte tecnológica, start-ups, procesos de selección y de entretenimiento",
    education: [
      {
        school: "Pontificia Universidad Católica del Perú",
        degree: "Ingeniería Informática",
        graduated: "En Diciembre 2020",
        description: "Egresado de Ingenieria Informática",
      },
    ],
    work: [
      {
        company: "Indra",
        title: "Technical Analyst | Frontend Developer",
        years: "Enero 2021 - Presente",
        description:
          "Analista Técnico asignado a dar soporte y mantenimiento a nuevas funcionalidades y proyectos diversos de los clientes de la empresa",
      },
      {
        company: "HC Planning",
        title: "MERN Developer Jr",
        years: "Agosto 2020 - Enero 2020",
        description:
          "Programador MERN Stack encargado de dar mantenimiento a la página web de la empresa",
      },
      {
        company: "Onfire.co",
        title: "Freelance - FullStack Developer Jr.",
        years: "Junio 2020 - Presente",
        description:
          "Programador FullStack Jr. encargado de desarrollar junto a un programador Senior distintas páginas propias de la empresa y de sus clientes.",
      },
      {
        company: "Globalnick",
        title: "FullStack Developer Jr.",
        years: "Diciembre 2019 - Marzo 2020",
        description:
          "Programador FullStack Jr. con funciones de desarrollar páginas de algunos clientes de la empresa",
      },
      {
        company: "Grupo Alta Montaña S.R.L",
        title: "App Developer",
        years: "Setiembre 2019 - Diciembre 2019",
        description:
          "Programador de un aplicativo móvil que imparte servicios de taxi. Elaborado con Ionic",
      },
      {
        company: "Lima Coders S.A.C",
        title: "Intern FullStack Developer",
        years: "Febrero 2019 - Agosto 2019",
        description:
          "Practicante FullStack encargado de desarrollar paginas web y ayudar en desarrollo de juegos",
      },
    ],
    skills: [
      {
        name: "ReactJs",
        level: "80%",
      },
      {
        name: "HTML5",
        level: "80%",
      },
      {
        name: "Vue",
        level: "70%",
      },
      {
        name: "Firebase",
        level: "70%",
      },
      {
        name:
          "AWS ( EC2, RDS, S3, Cognito, Amplify, Rekognition, Lambda, APIGateway )",
        level: "70%",
      },
      {
        name: "Express",
        level: "65%",
      },
      {
        name: "MongoDB",
        level: "60%",
      },
      {
        name: "MySql",
        level: "60%",
      },
      {
        name: "SQLServer",
        level: "60%",
      },
      {
        name: "CSS",
        level: "60%",
      },
      {
        name: "Git",
        level: "60%",
      },
      {
        name: "Figma",
        level: "55%",
      },
    ],
  },
  portfolio: {
    projects: [
      {
        title: "",
        category: "",
        image: "",
        url: "",
      },
      {
        title: "",
        category: "",
        image: "",
        url: "",
      },
      {
        title: "",
        category: "",
        image: "",
        url: "",
      },
      {
        title: "",
        category: "",
        image: "",
        url: "",
      },
      {
        title: "",
        category: "",
        image: "",
        url: "",
      },
    ],
  },
  testimonials: {
    testimonials: [
      {
        text: "",
        user: "",
      },
      {
        text: "",
        user: "",
      },
    ],
  },
};
