import React from "react";
import ReactGA from "react-ga";
import "./App.css";
import About from "./Components/About";
import Footer from "./Components/Footer";
import Header from "./Components/Header";
import Resume from "./Components/Resume";
import { resumeData } from "./resumeData";

const App = () => {
  ReactGA.initialize("UA-110208118-1");
  ReactGA.pageview(window.location.pathname);

  return (
    <div className="App">
      <Header data={resumeData.main} />
      <About data={resumeData.main} />
      <Resume data={resumeData.resume} />
      {/* <Portfolio data={resumeData.portfolio} /> */}
      {/* <Testimonials data={resumeData.testimonials} /> */}
      {/* <Contact data={resumeData.main} /> */}
      <Footer data={resumeData.main} />
    </div>
  );
};

export default App;
