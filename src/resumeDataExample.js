export const resumeData = {
  main: {
    name: "",
    occupation: "",
    description: "",
    image: "",
    bio: "",
    contactmessage: "",
    email: "",
    phone: "",
    address: {
      street: "",
      city: "",
      state: "",
      zip: "",
    },
    website: "",
    resumedownload: "",
    social: [
      {
        name: "facebook",
        url: "",
        className: "fa fa-facebook",
      },
      {
        name: "linkedin",
        url: "",
        className: "fa fa-linkedin",
      },
      {
        name: "instagram",
        url: "",
        className: "fa fa-instagram",
      },
      {
        name: "github",
        url: "",
        className: "fa fa-github",
      },
    ],
  },
  resume: {
    skillmessage: "",
    education: [
      {
        school: "",
        degree: "",
        graduated: "",
        description: "",
      },
    ],
    work: [
      {
        company: "",
        title: "",
        years: "",
        description: "",
      },
      {
        company: "",
        title: "",
        years: "",
        description: "",
      },
    ],
    skills: [
      {
        name: "ReactJs",
        level: "80%",
      },
    ],
  },
  portfolio: {
    projects: [
      {
        title: "",
        category: "",
        image: "",
        url: "",
      },
      {
        title: "",
        category: "",
        image: "",
        url: "",
      },
      {
        title: "",
        category: "",
        image: "",
        url: "",
      },
      {
        title: "",
        category: "",
        image: "",
        url: "",
      },
      {
        title: "",
        category: "",
        image: "",
        url: "",
      },
    ],
  },
  testimonials: {
    testimonials: [
      {
        text: "",
        user: "",
      },
      {
        text: "",
        user: "",
      },
    ],
  },
};
